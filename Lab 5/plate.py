from pyb import Pin, ADC
from math import pi
from time import ticks_us, ticks_diff
import micropython
from array import array

class Touchpad:
    '''!@brief      Contains methods to obtain specific data from encoder.
        @details    Creates timers used to obtain the position, delta, and to 
                    zero the encoder.
    '''
    
    @micropython.native
    def __init__ (self, xpPin, xmPin, ypPin, ymPin, xwidth, ylength):
        
        '''! @brief Creates the initial setup for the encoder
             @details Sets up all channels with the Nucleo
             @param pinB6 A pin used by the Nucleo for the encoder
             @param pinB7 A pin used by the Nucleo for the encoder
             @param tim4 The timer used for this specific encoder
        '''
        
        self.xpPin = xpPin
        self.xmPin = xmPin
        self.ypPin = ypPin
        self.ymPin = ymPin
        self.OUT = Pin.OUT_PP
        self.IN = Pin.IN
        self._res = micropython.const(4096)
        self.xscale = xwidth/self._res
        self.yscale = ylength/self._res
        self._half = micropython.const(2)
        self.xc = xwidth/self._half
        self.yc = ylength/self._half
        #initial values of calibration coefficients
        self.Kxx = 1
        self.Kxy = 0
        self.xo = 0
        self.Kyx = 0
        self.Kyy = 1
        self.yo = 0
        self._buf = array('h',25*[0])
        self._freq = micropython.const(200000)
        self._avgdiv = micropython.const(25)

    @micropython.native
    def xScan(self):
        
        '''! @brief Constantly updates the counter that tracks position.
             @details The position is tracked with a 16-bit counter.  To prevent
                      the counter from overflowing, a change, or delta between
                      previous counts is computed.  If the magnitude of the delta 
                      is larger than half of the maximum count (indicating that  
                      the counter reset), the delta is corrected such that the reset 
                      isn't seen.  Adding up all the deltas gives the current position.  
        '''
        
        xp = Pin(self.xpPin, self.OUT)
        xp.high()
        
        xm = Pin(self.xmPin, self.OUT)
        xm.low()
        
        yp = Pin(self.ypPin, self.IN)
        
        ym = ADC(Pin(self.ymPin))
        
        ym_rt = ym.read_timed(self._buf, self._freq)
        
        ym_filt = sum(self._buf)/self._avgdiv
    
        self.xADC = ym_filt*self.xscale - self.xc
        return self.xADC
#        self.xADC = ym.read()
        
    @micropython.native
    def yScan(self):
        
        '''! @brief Constantly updates the counter that tracks position.
             @details The position is tracked with a 16-bit counter.  To prevent
                      the counter from overflowing, a change, or delta between
                      previous counts is computed.  If the magnitude of the delta 
                      is larger than half of the maximum count (indicating that  
                      the counter reset), the delta is corrected such that the reset 
                      isn't seen.  Adding up all the deltas gives the current position.  
        '''
        
        yp = Pin(self.ypPin, self.OUT)
        yp.high()
        
        ym = Pin(self.ymPin, self.OUT)
        ym.low()
        
        xp = Pin(self.xpPin, self.IN)
        
        xm = ADC(Pin(self.xmPin))
        
        xm_rt = xm.read_timed(self._buf, self._freq)
        
        xm_filt = sum(self._buf)/self._avgdiv
        
        self.yADC = xm_filt*self.yscale - self.yc
        return self.yADC
#        self.yADC = xm.read()
        
    @micropython.native
    def zScan(self):
        
        '''! @brief Constantly updates the counter that tracks position.
             @details The position is tracked with a 16-bit counter.  To prevent
                      the counter from overflowing, a change, or delta between
                      previous counts is computed.  If the magnitude of the delta 
                      is larger than half of the maximum count (indicating that  
                      the counter reset), the delta is corrected such that the reset 
                      isn't seen.  Adding up all the deltas gives the current position.  
        '''
        
        yp = Pin(self.ypPin, self.OUT)
        yp.high()
        
        xm = Pin(self.xmPin, self.OUT)
        xm.low()
        
        xp = Pin(self.xpPin, self.IN)
        
        ym = ADC(Pin(self.ymPin))
        
        if ym.read() < 4050:
            self.z = 1
        else:
            self.z = 0
        
        return self.z
    
    @micropython.native    
    def XYZ_Scan(self):
        
        '''! @brief Constantly updates the counter that tracks position.
             @details The position is tracked with a 16-bit counter.  To prevent
                      the counter from overflowing, a change, or delta between
                      previous counts is computed.  If the magnitude of the delta 
                      is larger than half of the maximum count (indicating that  
                      the counter reset), the delta is corrected such that the reset 
                      isn't seen.  Adding up all the deltas gives the current position.  
        '''
        xsc = self.xScan()
        self.zScan()
        ysc = self.yScan()
        self.x = self.Kxx*xsc + self.Kxy*ysc + self.xo
        self.y = self.Kyx*xsc + self.Kyy*ysc + self.yo
        return(self.x, self.y, self.z)
        
    @micropython.native
    def set_cal_coeff(self, Kxx, Kxy, xo, Kyx, Kyy, yo):
        
        '''! @brief Constantly updates the counter that tracks position.
             @details The position is tracked with a 16-bit counter.  To prevent
                      the counter from overflowing, a change, or delta between
                      previous counts is computed.  If the magnitude of the delta 
                      is larger than half of the maximum count (indicating that  
                      the counter reset), the delta is corrected such that the reset 
                      isn't seen.  Adding up all the deltas gives the current position.  
        '''
        self.Kxx = Kxx
        self.Kxy = Kxy
        self.xo = xo
        self.Kyx = Kyx
        self.Kyy = Kyy
        self.yo = yo

        
if __name__ == '__main__':
    x = Touchpad(Pin.cpu.A7, Pin.cpu.A1, Pin.cpu.A6, Pin.cpu.A0, 176, 100)
    import utime
    n = 0
    t1 = utime.ticks_us()
    while n < 100:
        x.XYZ_Scan()
        n += 1
    t2 = utime.ticks_us()
#    print(t1)
#    print(t2)
    t = utime.ticks_diff(t2, t1)/100
    print(f'Run time for all 3 Scans: {t} microseconds')