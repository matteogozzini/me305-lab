from pyb import I2C
import time
import utime
import os
import struct
import pyb


class BNO055():
    
    def __init__ (self):
        self.i2c = I2C(1, I2C.CONTROLLER)
        self.adress = 0x28
        self.op_mode = 0x3D
        self.eulerArray = bytearray(6)
        self.calibCoefArray = bytearray(22)
        self.gyroArray = bytearray(6)
        self.i2c.mem_write(0, 0x28, 0x3D)
        self.i2c.mem_write(0x21, 0x28, 0x41)
        self.i2c.mem_write(0x02, 0x28, 0x42)
        
    
    def operating_mode(self, mode):
        self.i2c.mem_write(mode, 0x28, 0x3D)
        
    
    def get_calib_stat(self):
        buf = bytearray(1)
        self.i2c.mem_read(buf, 0x28, 0x35)
        cal_status = (buf[0] & 0b00000011, 
                      (buf[0] & 0b00001100)>>2, 
                      (buf[0] & 0b00110000)>>4, 
                      (buf[0] & 0b11000000)>>6)
        return cal_status
    
    def get_cal_coeff (self, calbuf):
        '''!@brief Brings the DRV8847 out of sleep mode.
            @details Enables DRV8847.  Temporarily disables fault triggering to fix
                     a bug in which the sensor would fault upon enabling.  
        '''        
        self.i2c.mem_read(calbuf, self.adress, 0x55)
        return calbuf
    
    def write_calib_coef(self, calib_coef):
        self.i2c.mem_write(calib_coef, 0x28, 0x55)
    
    def read_euler_ang(self):
        
        self.i2c.mem_read(self.eulerArray, 0x28, 0x1A)
        head = (self.eulerArray[1]<<8)|self.eulerArray[0]
        roll = (self.eulerArray[3]<<8)|self.eulerArray[2]
        pitch = (self.eulerArray[5]<<8)|self.eulerArray[4]
        if head > 32767:
            head -= 65536
        if roll > 32767:
            roll -= 65536
        if pitch > 32767:
            pitch -= 65536
        return (-head/16,-roll/16,-pitch/16)
    
    def read_ang_vel(self):
        self.i2c.mem_read(self.gyroArray, 0x28, 0x14)
        gyr_x = (self.gyroArray[1]<<8)|self.gyroArray[0]
        gyr_y = (self.gyroArray[3]<<8)|self.gyroArray[2]
        gyr_z = (self.gyroArray[5]<<8)|self.gyroArray[4]
        if gyr_x > 32767:
            gyr_x -= 65536
        if gyr_y > 32767:
            gyr_y -= 65536
        if gyr_z > 32767:
            gyr_z -= 65536
        return (gyr_x/16,gyr_y/16,gyr_z/16)
    
    