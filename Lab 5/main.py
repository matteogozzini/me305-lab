
import User_task5
import Motor_task5
import Controller_task5
import IMU_task5
import shares5
import MotorDriver5
import IMU
import closedloop5
import task_plate
import plate
from pyb import Pin


duty1 = shares5.Share(0)


duty2 = shares5.Share(0)

# tuple for calib status
calStat = shares5.Share((0,0,0,0))

# tuple for euler angles
eulAng = shares5.Share((0,0,0))

# tuple for gyr angular velocities
gyrVel = shares5.Share((0,0,0))


KpShare = shares5.Share(0)
KdShare = shares5.Share(0)


KiShare = shares5.Share(0)

KpOut = shares5.Share(0)
KdOut = shares5.Share(0)
KiOut = shares5.Share(0)


yShare = shares5.Share(0)


YSHARE = shares5.Share(0)



yFlag = shares5.Share(False)


wFlag = shares5.Share(False)
dFlag = shares5.Share(False)
DFlag = shares5.Share(False)

Pos = shares5.Share((0,0,0))

abfShare = shares5.Share((0,0,0,0,0)) # positions & velocities


motor_1 = MotorDriver5.Motor(3, Pin.cpu.B4, Pin.cpu.B5, 1, 2)


motor_2 = MotorDriver5.Motor(3, Pin.cpu.B0, Pin.cpu.B1, 3, 4)

# bno driver object
bno_obj = IMU.BNO055()

touch = plate.Touchpad(Pin.cpu.A7, Pin.cpu.A1, Pin.cpu.A6, Pin.cpu.A0, 176, 100)

CLC_O1 = closedloop5.ClosedLoop(0, 0, 0, yShare, 12, -12)
CLC_O2 = closedloop5.ClosedLoop(0, 0, 0, yShare, 12, -12)
CLC_I1 = closedloop5.ClosedLoop(0, 0, 0, yShare, 45, -45)
CLC_I2 = closedloop5.ClosedLoop(0, 0, 0, yShare, 45, -45)

if __name__ == '__main__':
    
   
    taskList = [User_task5.taskUserFCN ('Task User', 50_000, eulAng, gyrVel, duty1, duty2, KpShare, KdShare, KiShare, KpOut, KdOut, KiOut, wFlag, dFlag, DFlag, abfShare, yShare, YSHARE, yFlag),
                Motor_task5.motorFunction ('Task Motor 1', 10_000, motor_1, duty1),
                Motor_task5.motorFunction ('Task Motor 2', 10_000, motor_2, duty2),
                Controller_task5.Ctask('Task Motor Control 1', 10_000, CLC_O1, CLC_I1, eulAng, wFlag, KpShare, KdShare, KiShare, KpOut, KdOut, KiOut, duty1, gyrVel, 1, 1, dFlag, DFlag, abfShare, 0, 1, yShare, yFlag),
                Controller_task5.Ctask('Task Motor Control 2', 10_000, CLC_O2, CLC_I2, eulAng, wFlag, KpShare, KdShare, KiShare, KpOut, KdOut, KiOut, duty2, gyrVel, 2, 0, dFlag, DFlag, abfShare, 2, 3, YSHARE, yFlag),

                IMU_task5.bnoFunction('Task IMU', 10_000, calStat, bno_obj, eulAng, gyrVel),
                task_plate.Touch('Task Touchpad', 10_000, Pos, touch, 0.85, 0.005, abfShare)]
    
    while True:
        try:
            for task in taskList:
                next(task)
        except KeyboardInterrupt:
            KpShare.write(0)
            KdShare.write(0)
            duty1.write(0)
            duty2.write(0)
            break
    print('Stopping Motor')