
import os
import time
from time import ticks_add, ticks_diff, ticks_us, ticks_ms
from pyb import I2C
import micropython
import gc

S0_INIT = micropython.const(0)

S1_RUN = micropython.const (1)

S2_CALIB = micropython.const (2)

S3_SAVE_CAL_COEFFS = micropython.const (3)

S4_WRITE_CAL_COEFFS = micropython.const (4)

def bnoFunction(taskName, period, calStat, bno_obj, eulAng, gyrVel):
    '''! @brief Generator function that passes the duty cycles to the motor.
         @details This function passes the duty cycle to the method setduty which then
                  sends the value to the motor. 
         @param taskName Names the task so that multiple tasks can be run simultaneously.
         @param period Passes in the period at which the code is run.
         @param calStat A tuple containing 
         @param bno_obj 
    '''
    
   
    state = S0_INIT
    
   
    start_time = ticks_us()
    
    
    next_time = ticks_add(start_time, period)
    
    
    BNO = bno_obj
    
    filename = "IMU_cal_coeffs.txt"
    
    filestring = ''
    
    n = 0
     
    while True:
    
        
        current_time = ticks_us()
        if ticks_diff(current_time, next_time) >= 0:
            
            if state == S0_INIT:
                
                if filename in os.listdir():
                    # File exists, read from it
                    state = S4_WRITE_CAL_COEFFS
                else:
                    # File doesnt exist, calibrate manually and 
                    # write the coefficients to the file
                    
                    print('--- IMU Calibration Interface ---')
                    print('To calibrate the BNO055 IMU, you will need to rotate'
                          'the platform about its center axis until all 4' 
                          'parameters, mag, acc, gyr, and sys have a value of'
                          '3.')
                    print('Be ready to calibrate in 3 seconds')
                    BNO.operating_mode(12)
                    getReady()
                    state = S2_CALIB
                                
                
            elif state == S1_RUN:                
                eulAng.write(BNO.read_euler_ang())
                head, pitch, roll = eulAng.read()
                gyrVel.write(BNO.read_ang_vel())
                gyr_x,gyr_y,gyr_z = gyrVel.read()
                
                
                    
            elif state == S2_CALIB:
                calStat.write(BNO.get_calib_stat())
                mag, acc, gyr, sys = calStat.read()
                print('mag: ' +str(mag), 'acc: ' +str(acc), 'gyr: ' +str(gyr), 
                      'sys: ' +str(sys))
                if (mag,acc,gyr,sys) == (3,3,3,3):  
                    print('--- End Calibration ---')
                    print('s2 -> s3')
                    state = S3_SAVE_CAL_COEFFS
                    
            elif state == S3_SAVE_CAL_COEFFS:    
                with open(filename, 'w') as f:
                    # Perform manual calibration
                    buf = bytearray(22*[0])
                    gc.collect()
                    cal_co_barr = BNO.get_cal_coeff(buf)
                    print(cal_co_barr)
                    
                    for byte in cal_co_barr:
                        filestring += hex(byte)
                        filestring += ','
                        n += 1
                                                   
                    if n == 22:
                        print(filestring)
                        print('writing to file')
                        filestring = filestring[:-1]
                        f.write(filestring)
                        state = S1_RUN
                        print('s3 -> s1')
                        
            elif state == S4_WRITE_CAL_COEFFS:
                    
                with open(filename, 'r') as f:
                    # Read the first line of the file
                    cal_data_string = f.readline()
                    # Split the line into multiple strings
                    # and then convert each one to a float
                    calib_coef = bytearray([int(cal_value) for cal_value in cal_data_string.strip().split(',')])
                    print(calib_coef)
                    BNO.operating_mode(0)
                    BNO.write_calib_coef(calib_coef)
                state = S1_RUN
                print('s4 -> s1')
                BNO.operating_mode(12)
                    
            
            else:
                pass
            
            next_time = ticks_add(next_time, period)
                
            yield state

        else:
            
            yield None
            

def getReady():
    '''
    @brief Gives three ready statements as a buffer before calibration

    '''
    start_time = ticks_ms()
    cdn = 3 # countdown number
    while cdn >= 0:
        round_time = ticks_ms()
        if ((ticks_diff(round_time,start_time)) > 1000):
            if cdn == 3:
                print('Get ready')
            elif cdn == 2:
                print('Get set')
            elif cdn == 1:
                print('Calibrate')
            elif cdn == 0:
                print('\n')
            start_time = ticks_ms()
            cdn -= 1