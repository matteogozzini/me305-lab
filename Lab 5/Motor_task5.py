"""!@file           Motor_task.py
    @brief          Assigns pins to motors, checks for user input to disable a fault, and sets the duty cycle for the motors
    @details        assigns the correct pins to each motor, sets motor duty cycle based off user input, and checks to see if user wants to disable and motor fault 
    @author         Matteo Gozzini
    @date           2/17/22
"""
import time
from time import ticks_us, ticks_add, ticks_diff
import micropython

S0_INIT = micropython.const(0)

S1_WAIT = micropython.const (1)


    
def motorFunction(taskName, period, motor, dutyVal):
        
        '''@brief       Constructs a motor task object.
           @details     Instantiates the period, shared data, and motor driver.
           @param       period The frequency at which the motor is updated within main.py
           @param       Shares The shared data between all the tasks and drivers
           @param       motor_drv  A DRV8847 object
        '''
        
        state = S0_INIT
    
   
        start_time = ticks_us()
    
    
        next_time = ticks_add(start_time, period)
    
    
        motorNum = motor
     
        while True:
    
        
            current_time = ticks_us()
            if ticks_diff(current_time, next_time) >= 0:
            
                if state == S0_INIT:
                    state = S1_WAIT                
                
                elif state == S1_WAIT:
                    motorNum.set_duty(dutyVal.read())
                    
                else:
                    pass
            
                next_time = ticks_add(next_time, period)
                
                yield state

            else:
            
                yield None