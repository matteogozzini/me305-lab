"""!@file           Controller_task.py
    @brief          Main Program 
    @details        This main script utilizes the task files (user_task, motor_task & encoder_task) in order to 
                    communicate with the motor encoder through the encoder_lab3.py file with a set frequency 
                    for the update rate in order to get the encoders position
    @author         Matteo Gozzini
    @date           2/17/22
"""

import utime
import time
from time import ticks_add, ticks_diff, ticks_us
import micropython
from micropython import const

S0_INIT = micropython.const(0)
S1_RUN_OUTER = micropython.const (1)
S2_RUN_INNER = micropython.const (2)



def Ctask(taskName, period, CLC_OUT, CLC_IN, eulAng, wFlag, KpShare, KdShare, KiShare, KpOut, KdOut, KiOut, duty, gyrVel, numEul, numGyr, dFlag, DFlag, abfShare, posIdx, velIdx, RefVal, yFlag):
    '''!@brief      This generator function runs at period [period] and manages
                    the ClosedLoop object [controller]
        @details
        @param      controller An instance of the closedloop.ClosedLoop class to
                    be managed by this task.
        @param      K_p A shared variable used to pass the proportional gain from
                    the user task to this task.
        @param      K_i A shared variable used to pass the integral gain from
                    the user task to this task.
        @param      K_d A shared variable used to pass the derivative gain from
                    the user task to this task.
        @param      reference A shared variable used to pass the reference value
                    (setpoint) from the user task to this task.
        @param      position A shared variable representing the angle of the platform in one axis.
        @param      velocity A shared variable representing the angular velocity of the platform in one axis.
        @param      closedloopON A shared boolean variable indicating whether
                    closed loop control should be on.
        @param      period The period at which this task should run, in microseconds.
    '''
    
    
    state = S0_INIT
    
    starttime = ticks_us()
    nexttime = ticks_add(starttime, period)
    duty_val = duty
    
    
    while True:
        currenttime = ticks_us()
        if ticks_diff(currenttime, nexttime) >= 0:
            
            if state == S0_INIT:
                state = S1_RUN_OUTER 
                
            elif state == S1_RUN_OUTER:
                if wFlag.read():
                    #Run outer                    
                    CLC_OUT.set_Gain(KpOut.read(), KdOut.read(), KiOut.read())
                    act_sig_out = CLC_OUT.run(abfShare.read()[posIdx], abfShare.read()[velIdx], 0, 0, abfShare.read()[4])
                    if not yFlag.read():
                        RefVal.write(act_sig_out)
                    state = S2_RUN_INNER
                
            
            elif state == S2_RUN_INNER:
                CLC_IN.set_Gain(KpShare.read(), KdShare.read(), KiShare.read())
#                CLC_IN.set_Reference(RefVal.read())
                act_sig = CLC_IN.run(eulAng.read()[numEul], gyrVel.read()[numGyr], RefVal.read(), 1, 1)                    
                duty_val.write(act_sig)
                if dFlag.read():
                    if wFlag.read():
                        if taskName == 'Task Motor Control 1':
                            print(f'{taskName}: {act_sig}')
                            dFlag.write(False)
                if DFlag.read():
                    if wFlag.read():
                        if taskName == 'Task Motor Control 2':
                            print(f'{taskName}: {act_sig}')
                            DFlag.write(False)
                state = S1_RUN_OUTER 
                
            else:
                pass
            
            
            nexttime = ticks_add(nexttime, period)
                
            yield state

        else:
            
            yield None