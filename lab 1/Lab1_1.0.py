"""!
@file       Lab1_1.0.py
@brief      Cycles through various LED pulse patterns on STM32 nucleo
@details    This program cycles through 3 active states and one starting 
                state that takes the initial input. Once first executed, this 
                code will prompt the user to press the blue button on the 
                STM32-nucleo board. From there the LED on the board will switch
                on and off through the SquareWave function displaying either 
                full or no brightness depending on time. Once the blue button 
                is pressed again it will transistion to a SawWave function that 
                will increase the brightness of the LED one percent per a preset 
                time interval. Once the max brightness is reached the LED will 
                reset to zero brightness and build up to the max again. 
                Once the blue button is pressed again, the LED will transistion
                to a SineWave function where the LED oscillates between max and
                minimum brightness based upon a Sine function that takes time 
                as the dependant variable. Once the blue button is pressed
                again, the LED will go back to the SquareWave function and the 
                process can be repeated again.    
                
@brief          See my Source Code             https://bitbucket.org/matteogozzini/me305-lab/src/master/lab%201/Lab1_1.0.py
                 
@brief          See my code run                https://youtu.be/7CuT-mUEe5U
                 
@brief          see my Finite State Machine    https://imgur.com/gallery/Xrx9lcx
                 
@author         Matteo Gozzini
@date           1/17/22
               
    
"""

import pyb
import utime
import math


LED = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP) #creates variable for LED
button = pyb.Pin(pyb.Pin.cpu.C13) #creates variable for the blue button

def SquareWave(timeSinceStart):
    '''!@brief      Returns the value of a square wave, either 0 or 100
        @details    This function takes the amount of time spanned as its only 
                    input. It will return a value of 0 or 100 with a period of
                    1 second
        @param      timeSinceStart: this variable is the spanned time since an
                    arbitrary start time
        @return     The returned value is desired brightness of the LED, 
                    either 0 or 100. 
    
    '''
    
    remainder = timeSinceStart%1000 #modulous of arbitrary time since start in ms
    if remainder < 500:
        brightness = 100 #led brightness
    else:
        brightness = 0
    return brightness


def SawWave(timeSinceStart):
    '''!@brief      Returns the value of a saw tooth wave given a time              
        @details    Its only input is the amount of time that has passed
                    after an arbitrary start time. Returns a value that varies 
                    between 0 and 100. The function has a period of 1s
        @param      timeSinceStart: this variable is the spanned time since an
                    arbitrary start time
        @return     Returns the brightness peercent of the LED between 0 and
                    100 at a given time
    '''
    brightness = (timeSinceStart%1000)/10
    return brightness


def SineWave(timeSinceStart):
    '''!@brief      Returns value of a sine wave between 0 and 100 given a time input
        @details    This function takes time as its only input. More specifically it
                    takes the time spanned between an arbitrary start time. It 
                    returns the value of a sine wave that varies between 0 to 100
                    and has a period of 10s
        @param      timeSinceStart: this variable is the spanned time since an
                    arbitrary start time
        @return     Returns the brightness peercent of the LED between 0 and
                    100 at a given time
    '''
    brightness = 100*(0.5+0.5*math.sin(2*math.pi*timeSinceStart/10000))
    return brightness
    
    
def onButtonPressFcn(IRQ_src):
    '''!@brief      Updates the status of the button variable when the interupt
                    is triggered
        @details    This function is called when the interrupt "ButtonInt" is
                    triggered by the user pressing the blue button. It takes 
                    no parameters and returns no values. It only sets the global
                    variable "buttonIsPressed" to "True".
    '''
    
    global buttonIsPressed
    buttonIsPressed = True
    
   
ButtonInt = pyb.ExtInt(button,mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, callback=onButtonPressFcn)    


if __name__ == '__main__':
    buttonIsPressed = False
    
    tim2 = pyb.Timer(2, freq = 20000)
    
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=LED)
    
    start_time = utime.ticks_ms()
    
    state = 0
    print('press the blue button')
    
    while True:
        try:
            
            if state == 0:
                brightness = 100
                
                if buttonIsPressed:
                    state = 1
                    buttonIsPressed = False
                    print('Square Wave selected')
                    
            elif state == 1:
                timeSinceStart = utime.ticks_diff(utime.ticks_ms(), start_time)
                brightness = SquareWave(timeSinceStart)
                t2ch1.pulse_width_percent(brightness)
                if buttonIsPressed:
                    state = 2
                    buttonIsPressed = False
                    print('Saw Wave selected')
                        
            elif state == 2:
                timeSinceStart = utime.ticks_diff(utime.ticks_ms(), start_time)
                brightness = SawWave(timeSinceStart)
                t2ch1.pulse_width_percent(brightness)
                if buttonIsPressed:
                    state = 3
                    buttonIsPressed = False
                    print('Sine Wave selected')
                    
            elif state == 3:
                timeSinceStart = utime.ticks_diff(utime.ticks_ms(), start_time)
                brightness = SineWave(timeSinceStart)
                t2ch1.pulse_width_percent(brightness)
                if buttonIsPressed:
                    state = 1
                    buttonIsPressed = False
                    print("Square Wave selected")                  
                
        except KeyboardInterrupt:
            print("program Terminating")
            break