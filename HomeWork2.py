'''!@file HomeWork2.py

@page hw_2_page Ball and Plate Simulation Hand Calculations
@details        Here are the hand calculations to derive the equations of motion for the ball and plate platform simulation.     
                Kinematics

    @image html HW201.png "Ball and plate kinematics part 1" 
    More Kinematics
    @image html HW202.png  "Ball and plate velocity and acceleration vectors" 
    <br>
    Kinetics
    @image html HW203.png 
    "Ball and plate kinetics part 1"
    More Kinetics    
    @image html HW204.png 
    "Ball and plate kinetics part 2; ball kinetics"
    <br>
    Equations of Motion
    @image html HW205.png 
    "Equations of motion"     

'''