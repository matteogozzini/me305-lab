'''!@file TermProjectPage.py

@page TermProject   Term Project Deliverables 
@details            This page contains the report, the FSM's, and plots for Term Project
    
   @section Intro
   In this lab we balanced a ball on a touch pad through the  use of an accelerometer, gyroscope, magnetometer, and closed loop control.
   This report highlights the performance of our system 
    
   source code: https://bitbucket.org/matteogozzini/me305-lab/src/master/TermProject/
        
   <br>Plate Balancing Ball Video: https://www.youtube.com/watch?v=EBrSuy6Zu8U <br>
   <br>User Interface and System Performance Video: https://www.youtube.com/watch?v=JKYsUrfh2Lk <br>
    
    
   Roll Angle vs Time Plot
   @image html timvsroll.png width=700
    
    
   Pitch Angle vs Time Plot
   @image html pitchvstimie.png width=700
    

    
   Y Location of the Ball vs Time
   @image html yvstime.png width=700
    
    
    
   X Location of the Ball vs Time
   @image html xvstim.png width=700
    
     
   Task Diagram 
   @image html termstatdiam.png width=700
   <br>

   Data Task
   @image html dtask.png width=700
   <br>
     
   Controller Task
   @image html conttask.png width=700
   <br>
     
   Motor Task
   @image html mtask.png width=700
   <br>
     
   Plate Task
   @image html platetask.png width=700
   <br>
     
   IMU task
   @image html imutask.png width=700
   <br>
    
   User Task
   @image html utask.png width=700
   <br>
    

'''
