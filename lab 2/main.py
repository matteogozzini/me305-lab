"""!@file           main.py
    @brief          Main Script for Encoder Lab
    @details        This code executes the task_encoder.py and task_user.py modules at set frequencies 
                    in order to communicate with the motor encoder via the functions in encoder.py module. 
                    @image html TaskDiaLab2.png
                    @image html encodertask.png
                    @image html usertask.png
                    @image html PosVsTime.png
    @brief          See my source code: https://bitbucket.org/matteogozzini/me305-lab/src/master/lab%202/
    @author         Matteo Gozzini
    @date           1/30/22
    
"""

import task_encoder
import task_user
    
if __name__ == '__main__':   
    """!@brief    Creates and updates encoder and user objects at set frequencies
    """
    ##Encoder task object with a period of 2 ms
    #
    encodertask = task_encoder.Task_Encoder(2)
    ##User task object with period of 10 ms
    #
    usertask = task_user.Task_User(10)
    ##based off user input 'zero' will determine whether encoder needs to be zeroed
    #
    zero = False
    
    while(True):
        
        try:
            ## 'update' constains the instantaneous encoder position and delta
            #
            update = encodertask.zero_update(zero)
            zero = usertask.zero_update(update)
        
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')
    



   
 

