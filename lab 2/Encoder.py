"""!@file           encoder.py
    @brief          A driver for reading/calculating encoder position and time 
    @details        This script sets up timers for encoders on the STM board
                    and calculates the position of the encoder even with over/under flow
                    @image html TaskDiaLab2.png
                    @image html encodertask.png
                    @image html usertask.png
                    @image html PosVsTime.png
    @brief          See my source code: https://bitbucket.org/matteogozzini/me305-lab/src/master/lab%202/
    @author         Matteo Gozzini
    @date           1/30/22
"""

import pyb

class Encoder:
    """!@brief       interfaces with the encoder
    
    """

    
    def __init__(self, pin1, pin2, timerID):
        """!@brief           Creates encoder object and assigns pins to their correct timers
            @param pin1      Pin used with encoder channel 1
            @param pin2      Pin used with encoder channel 2
            @param timerID   Timer channel to be used with pins
        """
        
        ##Total position of the encoder (ticks)
        #
        self.position = 0
        
        ##Encoder position correlated to the update
        #
        self.position1 = 0
        
        ##Encoder position correlated to the update
        #
        self.position2 = 0
        
        ##Difference in position (ticks) between the most recent update and the previous update
        #
        self.delta = 0
        
        ##Timer
        self.tim4 = pyb.Timer(timerID, prescaler = 0, period = (2**16)-1)
        ##Assigning pin 1 to channel 1
        self.tim4.channel(1, pyb.Timer.ENC_AB, pin=pin1)
        ##Assigning pin 2 to channel 2
        self.tim4.channel(2, pyb.Timer.ENC_AB, pin=pin2)
    
    def update(self):
        """!@brief       Updates encoder position and delta
            @return      returns encoder position
        """
        self.position1 = self.position2
        self.position2 = self.tim4.counter()
        self.delta = self.get_delta()
        
        if(self.delta < -32768):
            self.position += (self.delta + 65536)
        elif(self.delta > 32768):
            self.position += (self.delta - 65536)
        else: 
            self.position += self.delta
        return self.position
        
               
    def get_pos(self):
        """!@brief       Returns encoder position
            @return      encoder position
        """
        
        return self.position
    
    
    def zero(self, pos):
        """!@brief       Set the encoder position       
            @param pos   new position of the encoder
        """
        print('Setting position value')
        self.position = pos
        
    
    def get_delta(self):
        """!@brief      Calculates and returns the difference in ticks between the 2 most recent updates
            @return     The difference in encoder positions between last 2 updates
        """
        return(self.position2 - self.position1)
    
    