"""!@file          task_encoder.py
    @brief          Module that collects data from encooders
    @details        Communicates and shares data with encoder.py module 
                    Creates encoder objects and zeros the position when prompted or collects data from the encoder
                    @image html TaskDiaLab2.png
                    @image html encodertask.png
                    @image html usertask.png
                    @image html PosVsTime.png
    @brief          See my source code: https://bitbucket.org/matteogozzini/me305-lab/src/master/lab%202/
    @author         Matteo Gozzini
    @date           1/30/22
"""

import pyb
import encoder
import utime


## Assigned the pin B6 on the STM to the varibale 'pinB6'
pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
## Assigned the pin B7 on the STM to the varibale 'pinB7'
pinB7 = pyb.Pin(pyb.Pin.cpu.B7)

class Task_Encoder:
    """!@brief     Class that zeros, updates, and collects rotational position for encoders
    
    """
        
    def __init__(self, period):
        
        """!@brief           Makes Encoder Object     
           @param period    the period that then gets altered to a frequency for the task_encoder
        """
        
        ##creates encoder as an object
        self.encoder1 = encoder.Encoder(pinB6, pinB7, 4) 
        ##The period in which task_encoder updates the position
        self.period = period
        ##Current state of task_encoder (state zero)
        self.state = 0
        ##Timer that increases by the task period
        self.next_time = utime.ticks_add(utime.ticks_ms(), self.period)
        ##Holds encoder position and delta for each update
        self.lastupdate = (0,0)
    
    def zero_update(self, null):
        """!@brief           Updates encoder at a fixed period, zeros the encoder when user wants position zeroed       
           @param null      Boolean value that determines when eencoder should reset to zero given the user input
           @return          Most recent update of position and delta
        """    
        
        if null:
            ## zero function from encoder.py
            self.encoder1.zero(0)
            
            
        if (utime.ticks_diff(utime.ticks_ms(), self.next_time) >= 0):
            
            self.encoder1.update()
            
            self.lastupdate = self.transition()
            
        return self.lastupdate
    
    def transition(self):
        """!@brief           Adds period to self.next_time and returns encoder position and delta 
           @return          position and delta of most recent update
        """
        self.next_time += self.period
        
        return (self.encoder1.get_pos(), self.encoder1.get_delta())
        
                

