"""!@file          task_user.py
    @brief          Module that creates user interface
    @details        Interprets user input from given list of commands and determines whether to
                    zero the position, print position, print delta, collect data for 30s and then print,
                    or ends data collection early (before 30s) and prints all values untill stopped. 
                    @image html TaskDiaLab2.png
                    @image html encodertask.png
                    @image html usertask.png
                    @image html PosVsTime.png
    @brief          See my source code: https://bitbucket.org/matteogozzini/me305-lab/src/master/lab%202/
    @author         Matteo Gozzini
    @date           1/30/22
"""

import utime
import pyb

class Task_User:
    """!@brief       Creates command window for user inputs related to the motor encoder
    
    """
    
    
    def __init__(self, period):
        """!@brief           Initiates communication with task_encoder inorder to access encoder data and display it          
            @param period    period in which task_user is called upon
        """
        ##allows for communication between def __init__ and USB's command prompt
        self.Command = pyb.USB_VCP()
        ##Current state of user task
        self.state = 0
        ##Period of user task       
        self.period = period
        ##Variable that is used in order for zero_update function to know when to start        
        self.next_time = utime.ticks_add(self.period, utime.ticks_ms())
        
        ##Value that determines if write() should begin collecting data
        self.displayPosition = False
        
        ##List for time data 
        self.timelist = []
        ##List for position data
        self.positionlist = []
        ##30 seconds so that write() knows when to stop collecting data
        self.DisplayPositionTime = 30010
        ##Initial time
        self.timeBegin = 0
        
        
    def zero_update(self, update):
        """!@brief           Creates user interface and interprets inputs   
            @param update    List of encoder postion and delta values
            @return          T/F value for boolean variable zero in main.py
        """
        if(utime.ticks_diff(utime.ticks_ms(), self.next_time) >= 0):
            self.next_time += self.period
            if(self.state == 0):
                
                print('--------------------------------\n'
                      '          USER COMMAND          \n'
                      '--------------------------------\n'
                      'z: Set encoder position to zero \n'
                      'p: Print encoder position to command window \n'
                      'd: Print encoder delta to command window \n'
                      'g: Collect encoder data for 30 seconds, then print to command window\n'
                      's: End data collection early\n'
                      'ESC: Display user interface controls\n'
                      '--------------------------------')
        
                self.transition(1) 
            
            elif(self.state == 1):
        
                keyCommand = self.read()
                zero = self.write(keyCommand,update)
                return zero
        
        return False
    
    
    def write(self,keyCommand,update):
        """!@brief               Interprets user input and determines what to display based off input
            @param keyCommand    Corresponsto character inputted by user
            @param update        contains list of encoder position and delta values
            @return              T/F value for boolean variable zero in main.py
        """
        
        
        (pos,del_ticks) = update
        current_time = utime.ticks_ms()
        
        if keyCommand == b'z':
            return True
                
                
        if (self.displayPosition == True and (keyCommand == b's' or utime.ticks_diff(utime.ticks_add(self.timeBegin, self.DisplayPositionTime), current_time) <= 0)):
            print('Time [sec], Position [ticks]')
            for n in range(len(self.timelist)):
                print('{:}, {:}'.format(self.timelist[n]/1000,self.positionlist[n]))
            self.displayPosition = False
            self.timelist = []
            self.positionlist = []
            
        elif(self.displayPosition):
            self.timelist.append(utime.ticks_diff(current_time, self.timeBegin))
            self.positionlist.append(pos)
        
        elif keyCommand == b'g':
            print('Collecting data...')
            self.displayPosition = True
            self.timeBegin = current_time
            
        elif keyCommand == b'd':
            print('Encoder Delta: ' +str(del_ticks))
            
        elif keyCommand == b'p':
            print('Encoder Position: ' + str(pos))
            
        return False
            
            
    def read(self):
        """!@brief       Reads user input and returns that character to the zero_update function 
            @return      Either returns inputted character or displays user commands again
        """
        
        keyCommand = b''  
        
        if(self.Command.any()):
            
            keyCommand = self.Command.read(1)
            
            self.Command.read()
            
            if(keyCommand == b'\x1b'):
                self.transition(0)
                return ''
            
        return keyCommand
          
    def transition(self, new_state):
        """!@brief               Transitions to a new state 
            @param new_state     next state
        """
        self.state = new_state