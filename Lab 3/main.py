"""!@file           main.py
    @brief          Main Program 
    @details        This main script utilizes the task files (user_task, motor_task & encoder_task) in order to 
                    communicate with the motor encoder through the encoder_lab3.py file with a set frequency 
                    for the update rate in order to get the encoders position
    @author         Matteo Gozzini
    @date           2/17/22
"""

import encoder_task
import MotorDriver
import motor_task
import user_taskLab3
import shares

## Defines the period [ms] (and thus frequency) at which the task_encoder is run. In this case it is currently set at 2 ms.
per_encoder = 2
## Defines the period [ms] (and thus frequency) at which the task_user is run. In this case it is currently set at 40 ms.
per_user = 10
## Defines the period [ms] (and thus frequency) at which the task_user is run. In this case it is currently set at 2 ms.
per_motor = 2

    
if __name__ == '__main__':   
    ''' @brief    Creates objects for the encoder tasks and user tasks, and subsequently updates them at their respectively chosen frequencies.
        
    '''
    ## Motor 1 Shared Values 
    ##
    ##Shared values include the encoder number, encoder position, encoder velocity, assigned duty cycle, zero flag, and fault flag
    ShareM1 = shares.ShareMotor([1, 0, 0, 0, False, False])
    ## Motor 2 Shared Values 
    ##
    ##Shared values include the encoder number, encoder position, encoder velocity, assigned duty cycle, zero flag, and fault flag
    ShareM2 = shares.ShareMotor([2, 0, 0, 0, False, False])
    ## Instantiated motor driver object
    motor_drv = MotorDriver.Driver(3)
    ## Instantiate encoder task 1 object
    encoderTask1 = encoder_task.TaskEncoder(per_encoder, ShareM1)
    ## Instantiate encoder task 2 object
    encoderTask2 = encoder_task.TaskEncoder(per_encoder, ShareM2)
    ## Instantiate motor task 1 object
    motorTask1 = motor_task.TaskMotor(per_motor, ShareM1, motor_drv)
    ## Instantiate motor task 2 object
    motorTask2 = motor_task.TaskMotor(per_motor, ShareM2, motor_drv)
    ## Instantiate user task
    userTask = user_taskLab3.TaskUser(per_user, ShareM1, ShareM2)
    
    while(True):
        
        try: 
            
            encoderTask1.run()
            encoderTask2.run()
            
            userTask.run()
            
            motorTask1.run()
            motorTask2.run()
            
            
            
        except KeyboardInterrupt:
            break
    motor_drv.disable()
    print('Program Terminating')
    ## The task object associated with the LED blinking code
    



   
 

