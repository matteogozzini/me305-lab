"""!@file           MotorDriver.py
    @brief          File for the motor which contains the Driver and Motor Class that controls dduty cycle of the motor
    @details        Enables and disables the motor. Also checks for faults and returns motor objects 
    @author         Matteo Gozzini
    @date           2/17/22
"""

import pyb
import time

class Driver:
    ''' @brief A motor driver class for the Driver.
        @details   Objects of this class can be used to configure the Driver
                   motor driver and to create one or more objects of the
                   Motor class which can be used to perform motor
                   control.

         stuff for both motors at the same time


     '''
    
    def __init__(self, timChannel):
        ''' @brief    Initializes and returns a Driver object.
            @details  Creates a timer, pins, and external interrupt trigger that are used by both motors.
            @param    timChannel  The channel associated with the timer for the motors.

        '''
        
        ##  Timer for IN1 and IN2 pins
        self.tim4 = pyb.Timer(timChannel, freq = 20000)
       
        ## On or Off attribute that enables/disables motor driver
        self.PinSleep = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        
        ## On or Off attribute that triggers the driver fault condition
        self.Fault = pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.OUT_PP)
        
        ## External interrupt that is used to detect faults. If they are detected the callback function fault_cb disables the motor
        self.faultTrigger = pyb.ExtInt(self.Fault, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)
        

    def enable (self):
        ''' @brief Brings the Driver out of sleep mode.
        
        '''
        self.faultTrigger.disable()
        self.PinSleep.on()
        time.sleep(.025)
        self.faultTrigger.enable()
        

    def disable (self):
        ''' @brief Puts the Driver in sleep mode.
        '''
        self.PinSleep.off()
        

    def fault_cb (self, IRQ_src):
        '''@brief Callback function to run on fault condition.
           @param IRQ_src The source of the interrupt request.
        '''
        print('Fault detected; disabling motor...')
        self.disable()
    
        

    def motor (self, motor_pin1, motor_pin2, channel1, channel2):
        '''@brief Initializes and returns a motor object associated with the Driver.
           @param motor_pin1 Nucleo pin associated with the first motor pin
           @param motor_pin2 Nucleo pin associated with the second motor pin
           @param channel1 Nucleo pin associated with the first channel pin
           @param channel2 Nucleo pin associated with the first channel pin
           @return An object of class Motor
           
        '''
        
        
        return Motor(motor_pin1, motor_pin2, channel1, channel2, self.tim4)



class Motor:
    '''@brief A motor class for one channel of the Driver.
       @details Objects of this class can be used to apply PWM to a given
       DC motor.
    
    
    
    '''

    def __init__(self, motor_pin1, motor_pin2, channel1, channel2, timX):
        '''@brief Initializes and returns a motor object associated with the Driver.
           @details Objects of this class should not be instantiated
                 that to create Motor objects using the method
                 Driver.motor().
           @param motor_pin1 The first motor pin.
           @param motor_pin2 The second motor pin.
           @param channel1 The first channel associated with a motor.
           @param channel2 The second channel associated with a motor.
           @param timX The timer associated with the motor.
        '''
        ## Attribute that assigns CPU pin PB4 to driver IN1 pin
        pinIN1 = pyb.Pin(motor_pin1, pyb.Pin.OUT_PP)
        ## Attribute that assigns CPU pin PB5 to driver IN2 pin
        pinIN2 = pyb.Pin(motor_pin2, pyb.Pin.OUT_PP)
        
        
        
        
        ## Assigning the pin1 parameter to the STM32L476RG channel 1
        self.t3ch1 = timX.channel(channel1, pyb.Timer.PWM, pin=pinIN1)
        ## Assigning the pin2 parameter to the STM32L476RG channel 2
        self.t3ch2 = timX.channel(channel2, pyb.Timer.PWM, pin=pinIN2)
        
        
    def set_duty (self, duty):
         '''@brief Set the PWM duty cycle for the motor channel.
             
            @param duty A signed number holding the duty
            cycle of the PWM signal sent to the motor
         '''
         if duty >= 0:
             if duty <=100:
                
                self.t3ch1.pulse_width_percent(100)
                self.t3ch2.pulse_width_percent(100 - duty)
             else:
                self.t3ch1.pulse_width_percent(100)
                self.t3ch2.pulse_width_percent(0)
         elif duty <0:
             if duty >= -100:
                self.t3ch2.pulse_width_percent(100)
                self.t3ch1.pulse_width_percent(100 + duty)
             
             else:
                
                self.t3ch2.pulse_width_percent(1000)
                self.t3ch1.pulse_width_percent(0)
         
         

     
     
        