"""!@file mainpage.py
   @brief ME 305 lab assignments and homework assignments
   @mainpage
      


                

@section sec_intro    Introduction
         Links to the repository of various source codes and Lab documentation
         Source Code Repository:  https://bitbucket.org/matteogozzini/me305-lab/src/master/  <br>
         <br>
         <B>Lab0x01:</B><br> Documentation:  https://matteogozzini.bitbucket.io/dir_91f1c6fff99e55d087f973ba55bb1697.html  <br>
         LED Demonstration:  https://youtu.be/7CuT-mUEe5U <br>
         Finite State Machine: https://imgur.com/gallery/Xrx9lcx  <br>
         <br>
         <B>Lab0x02:</B><br> Documentation:  https://matteogozzini.bitbucket.io/dir_31051d3946a9e77657a18ee63eb6dec3.html  <br>
         @ref Lab2_GraphFSM
         <br>
         <br>
         <B>Lab0x03:</B><br>  Documentation: https://matteogozzini.bitbucket.io/dir_e65f9effa1f181a31f346f7622f3492c.html <br> 
         @ref lab_3_page 
         <br>
         <br>
         <B>Lab0x04:</B><br>  Documentation: https://matteogozzini.bitbucket.io/dir_2630def5d0538684e5946c8a36a9e4e6.html <br>
         @ref lab_4_page 
         <br>
         <br>
         <B>Term Project - Balancing Ball on Plate :</B><br>  Documentation: file:///Users/matteogozzini/Desktop/Staging%20Area%20-Doxygen/html/dir_28ff91af791232534083f7c71b5b155f.html <br>
         <br>Plate Balancing Ball Video: https://www.youtube.com/watch?v=EBrSuy6Zu8U <br>
         <br>User Interface and System Performance Video: https://www.youtube.com/watch?v=JKYsUrfh2Lk <br>
         @ref TermProject 
         <br>
         <br>
         <B>Homework 2:</B><br>
         @ref hw_2_page
         <br>
         <br>
         <B>Homework 3:</B><br>
         @ref hw_3_page
         
         
@author  Matteo Gozzini






"""

