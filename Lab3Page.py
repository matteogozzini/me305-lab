'''!@file Lab3Page.py

@page lab_3_page Deliverables For Lab 3
@details        This page contains all the FSM's and plots for Lab 3
    Task Diagram Task Diagram
    @image html TaskDiaLab3.png 
    <br>
    
    Encoder Task FSM
    @image html EncoderLab3.png 
    <br>
    
    User Task FSM
    @image html  UserLab3.png 
    <br>
    
    Motor Task FSM
    @image html  MotorLab3.png 
    <br>
    
    Position of Motor Vs Time
    @image html PosVStimeLab3.png 
    <br>
    Velocity of Motor Vs Duty Cycle
    @image html VelvsDuty.png 

'''