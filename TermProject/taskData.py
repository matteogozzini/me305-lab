'''!@file       taskData.py
    @brief      This file is the task responsible for collection of encoder
                position, delta, and speed data
    @details    This task file has two states: S0_DATAOFF and S1_DATAON. The
                DATAOFF state indicates that data collection is not ongoing,
                and the DATAON state indicates that data collection is ongoing.
                When the shared variable datarecord is set to True by the user
                task, the data task changes its state to DATAON and starts
                recording data. As long as datarecord is true, data is
                collected in the local arrays timearray, positionarray,
                and deltaarray. When data recording begins, the local arrays
                are preallocated to the desired size, based on the value of
                the shared variable datapoints. Data collection ends either
                when the local arrays fill up (timeout) or when the datarecord
                boolean is set to False by the user task.
    @image      html "Lab 3 Data Collection Task FSM.png" "Data Colelction Task Finite State Machine" width=960
    @author     Daniel Zevenbergen
    @author     Matteo Gozzini
    @date 02/13/2022
'''

import utime
from micropython import const
import gc

def Dtask(datarecord, dataFlag, datapoints, timeData, channel1, channel2, channel3, channel4, rollang, pitchang, rollvel, pitchvel, ballpos_X, ballpos_Y, ballvel_X, ballvel_Y, dutycycle1, dutycycle2, timeout, finalindex, period):
    '''!@brief      This generator function runs once every [period]
                    microseconds and is responsible for adding the current
                    time, encoder position, and encoder delta to the data arrays
                    timearray, positionarray, and deltaarray
        @param      datarecord A shared boolean variable activated by the user
                    task which, when True, indicates that data collection should
                    be ongoing.
        @param      dataFlag This shared boolean variable, when False, indicates
                    to the user task that data is not ready to be printed.
        @param      datapoints An integer representing the maximum number of
                    data points that may be collected before data collection is
                    automatically halted.
        @param      timeData A shared array variable for time values
        @param      channel1 A shared array variable for the first data channel
        @param      channel2 A shared array variable for the second data channel
        @param      channel3 A shared array variable for the third data channel
        @param      channel4 A shared array variable for the fourth data channel
        @param      rollang A shared variable representing the roll angle of the platform
        @param      pitchang A shared variable representing the pitch angle of the platform
        @param      rollvel A shared variable representing the roll velocity of the platform
        @param      pitchvel A shared variable representing the pitch velocity of the platform
        @param      ballpos_X A shared variable representing the x-position of the ball on the platform
        @param      ballpos_Y A shared variable representing the y-position of the ball on the platform
        @param      ballvel_X A shared variable representing the x-velocity of the ball on the platform
        @param      ballvel_Y A shared variable representing the y-velocity of the ball on the platform
        @param      dutycycle1 A shared variable representing the duty cycle of motor 1
        @param      dutycycle2 A shared variable representing the duty cycle of motor 2
        @param      timeout A shared boolean variable that is set to True to inform the user task that data collection has timed out
        @param      finalindex A shared variable representing the last index of the arrays storing the data
        @param      period The period at which the data task should run, in
                    microseconds.
    '''
    S0_DATAOFF = const(0)
    S1_DATAON = const(1)
    
    state = S0_DATAOFF
    
    starttime = utime.ticks_us()
    datastarttime = starttime
    nexttime = starttime
    index = 0
    
    
    while True:
        currenttime = utime.ticks_us()
        if utime.ticks_diff(nexttime, currenttime) <= 0:
            nexttime = utime.ticks_add(nexttime, period)
            
            if state == S0_DATAOFF:
                if datarecord.read():
                    index = 0
                    gc.collect()
                    datastarttime = utime.ticks_us()
                    dataFlag.write(False)
                    state = S1_DATAON
                yield None
            elif state == S1_DATAON:
                if index < datapoints:
                    timeData[index] = utime.ticks_diff(utime.ticks_us(), datastarttime)
                    channel1[index] = rollang.read()
                    channel2[index] = pitchang.read()
                    channel3[index] = ballpos_X.read()
                    channel4[index] = ballpos_Y.read()
                    index += 1
                else:
                    datarecord.write(False)
                    timeout.write(True)
                if not datarecord.read():
                    finalindex.write(index-1)
                    state = S0_DATAOFF
                    dataFlag.write(True)
                yield None
        else:
            yield None
                
            
            
            
            
            
            
            
            
            
            