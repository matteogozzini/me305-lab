'''!@file       User_task.py
    @brief      This file is responsible for taking and handling user inputs
                and presenting data to the user.
    @details    This task has a certain period and is run once per period.
                Each time it runs, it checks for a user input via the USB
                virtual comport and processes the input accordingly. An input
                of z, p, v, y, k, g, or s (capital or lowercase) will result in
                the task performing the corresponding action.
    @image      html UserTask_State_Transition_Diagram_Six.png "Lab 3 User Task Finite State Machine" width=960
    @author     Daniel Zevenbergen
    @author     Matteo Gozzini
    @date       02/02/2022
'''

import utime
from micropython import const
import gc
import array


def Utask(serport, dataFlag, pos, velocity, ballpos_X, ballpos_Y, ballvel_X, ballvel_Y, ball_z, dutycycle1, dutycycle2, reference, K_pOUT, K_iOUT, K_dOUT, K_pIN, K_iIN, K_dIN, closedloopON, datarecord, timeData, channel1, channel2, channel3, channel4, timeout, finalindex, calibstat, IMU_calibrated, Panel_calibrated, BNO055, period):
    '''!@brief      This function handles user inputs and performs the corresponding
                    tasks. It also handles printing data to the user interface.
        @details    This function is called by main and runs every [period]
                    microseconds. In its usual state "S1_WAIT", the task checks
                    the serial port for user inputs and, if any are present,
                    reads and decodes the input. It will set the state to the
                    proper state
                    depending on the user input. Once the
                    action has been completed, the task will return to the "WAIT" state.
                    The task also checks if the shared variable "timeout" has
                    been set to True, in which case the state will be set to
                    DATAEND to end data collection and print the data to
                    PuTTy.
        @param      serport This parameter is the serial port from which the
                    user task will read user input characters.
        @param      dataFlag This shared boolean variable, when False, indicates
                    to the user task that data is not ready to be printed.
        @param      pos This shared variable is used to pass the encoder
                    position from the Encoder task to the User task.
        @param      velocity This shared variable is used to pass the encoder
                    velocity from the encoder task to the user task.
        @param      ballpos_X A shared variable representing the x-position of the ball on the touch panel.
        @param      ballpos_Y A shared variable representing the y-position of the ball on the touch panel.
        @param      ballvel_X A shared variable representing the x-velocity of the ball on the touch panel.
        @param      ballvel_Y A shared variable representing the y-velocity of the ball on the touch panel.
        @param      ball_z A shared boolean variable that is True when there is contact with the touch panel.
        @param      dutycycle1 This shared variable is used to pass the duty
                    cycle for motor 1 from the user task to the motor task.
        @param      dutycycle2 This shared variable is used to pass the duty
                    cycle for motor 2 from the user task to the motor task.
        @param      reference A shared variable representing the setpoint for closed-loop control.
        @param      K_pOUT A shared variable representing the proportional gain for the outer control loop.
        @param      K_iOUT A shared variable representing the integral gain for the outer control loop.
        @param      K_dOUT A shared variable representing the derivative gain for the outer control loop.
        @param      K_pIN A shared variable representing the proportional gain for the inner control loop.
        @param      K_iIN A shared variable representing the integral gain for the inner control loop.
        @param      K_dIN A shared variable representing the derivative gain for the inner control loop.
        @param      closedloopON A shared boolean variable that is True when closed loop control is active.
        @param      datarecord This shared boolean variable is set to True to
                    indicate to the data collection task that data recording
                    should be ongoing. To halt data collection, the user task 
                    sets this variable to False.
        @param      timeData This shared variable is used to pass an array of
                    time data from the Encoder task to the User task.
        @param      channel1 An array containing channel 1 of data.
        @param      channel2 An array containing channel 2 of data.
        @param      channel3 An array containing channel 3 of data.
        @param      channel4 An array containing channel 4 of data.
        @param      timeout This shared variable is used to indicate to the
                    User task that data collection has timed out.
        @param      finalindex This shared variable indicates the index of the
                    last data point collected before data collection was ended.
        @param      calibstat A shared string containing the calibration status of the IMU.
        @param      IMU_calibrated A shared boolean variable that reads True when the
                    IMU is calibrated.
        @param      Panel_calibrated A shared boolean variable that reads True when
                    the touch panel is calibrated
        @param      BNO055 The BNO055 object representing the IMU
        @param      period This parameter, in microseconds, indicates to the
                    User task how often it is supposed to run.
    '''
    S0_INIT = const(0)
    S1_WAIT = const(1)
    S2_IMUCALIBRATE = const(2)
    S3_POSITION = const(3)
    S4_PANELCALIBRATE = const(4)
    S5_VELOCITY = const(5)
    S6_ZEROIMU = const(6)
    S9_DATASTART = const(9)
    S11_DATAEND = const(11)
    S12_SETPOINT = const(12)
    S13_GAINS = const(13)
    S14_INPUTNUM = const(14)
    S15_CLOSEDLOOP = const(15)
    
    
    state = S0_INIT
    starttime = utime.ticks_us()
    nextcalibdisptime = starttime
    nexttime = starttime
    buffer = ''
    toUpdate = None # variable to be updated by state 14
    upperlim = 0 # upper limit of numerical input to state 14
    lowerlim = 0 # lower limit of numerical input to state 14
    
    sFlag = False
    
    while True:
        currenttime = utime.ticks_us()
        if utime.ticks_diff(nexttime, currenttime) <= 0: # check if it's time to run the user task
            nexttime = utime.ticks_add(nexttime, period)
            # This conditional checks if the current state is S0_INIT and prints
            # the menu if it is
            if state == S0_INIT:
                print("Welcome to the program!")
                printmenu()
                if not IMU_calibrated.read():
                    state = S2_IMUCALIBRATE
                    print('1. Keep platform still until gyro is calibrated')
                    print('2. Move platform between different orientations until accelerometer is calibrated')
                    print('3. Shake platform wildly until magnetometer is calibrated')
                elif not Panel_calibrated.read():
                    state = S4_PANELCALIBRATE
                    print('Touch panel Calibration Required')
                    print('Touch lower left corner of panel')
                else:
                    state = S1_WAIT
                yield None
            
            # This conditional checks if the current state is S1_WAIT
            elif state == S1_WAIT:
                
                # This conditional checks if there are any characters waiting
                # to be read in the serport and, if so, decodes them and checks
                # if they are one of the user inputs. If they are, the state
                # is set accordingly. If not, the menu is printed again.
                if serport.any():
                    charIn = serport.read(1).decode()
                    if charIn in {'p', 'P'}:
                        print('Printing position...')
                        state = S3_POSITION
                    elif charIn in {'v', 'V'}:
                        print('Printing velocity...')
                        state = S5_VELOCITY
                    elif charIn in {'z', 'Z'}:
                        print('Zeroing IMU...')
                        state = S6_ZEROIMU
                    elif charIn in {'g', 'G'}:
                        print('Starting data collection')
                        state = S9_DATASTART
                    elif charIn in {'s', 'S'}:
                        print('Terminating data collection')
                        datarecord.write(False)
                        state = S11_DATAEND
                    elif charIn in {'y', 'Y'}:
                        state = S12_SETPOINT
                    elif charIn in {'k', 'K'}:
                        state = S13_GAINS
                    elif charIn in {'w', 'W'}:
                        print('Toggling closed loop control')
                        state = S15_CLOSEDLOOP
                    else:
                        print(f'You entered {charIn}.')
                        printmenu()
                # This conditional checks for timeout and sets the state to
                # DATAEND if timeout has occured.
                elif timeout.read():
                    timeout.write(False)
                    print('Data collection timeout')
                    state = S11_DATAEND
                yield None
            
            # The following set of conditionals check the current state of the
            # user task and raise the proper flag before resetting the state
            # to WAIT
            elif state == S2_IMUCALIBRATE:
                if IMU_calibrated.read():
                    print("IMU Calibrated!")
                    state = S0_INIT
                    yield None
                else:
                    if utime.ticks_diff(nextcalibdisptime, currenttime) <= 0:
                        nextcalibdisptime = utime.ticks_add(nextcalibdisptime, 1_000_000)
                        print("Calibration Status:")
                        print(calibstat.read())
                    yield None
            elif state == S3_POSITION:
                state = S1_WAIT
                print(f'IMU Position = {pos.read()}')
                print('Touch Panel Position = ')
                print(f'X Position = {ballpos_X.read()}')
                print(f'Y Position = {ballpos_Y.read()}')
                print(f'Contact = {ball_z.read()}')
                yield None
            elif state == S4_PANELCALIBRATE:
                if Panel_calibrated.read():
                    print("Touch Panel Calibrated!")
                    state = S1_WAIT
                    yield None
                else:
                    yield None
            elif state == S5_VELOCITY:
                state = S1_WAIT
                print (f'Velocity = {velocity.read()}')
                print('Touch Panel Velocity = ')
                print(f'X Velocity = {ballvel_X.read()}')
                print(f'Y Velocity = {ballvel_Y.read()}')
                print(f'Contact = {ball_z.read()}')
                yield None
            elif state == S6_ZEROIMU:
                state = S1_WAIT
                BNO055.zero()
                print('IMU Zeroed!')
            elif state == S9_DATASTART:
                state = S1_WAIT
                datarecord.write(True)
                print ('Data collection started')
                yield None
            elif state == S11_DATAEND:
                state = S1_WAIT
                while not dataFlag.read():
                    yield None
                print ('Data Collection terminated')
                for index in range(finalindex.read()):
                    print(f'{timeData[index]}, {channel1[index]}, {channel2[index]}, {channel3[index]}, {channel4[index]}')
                    yield None
                finalindex.write(0)
                gc.collect()
                yield None
            elif state == S12_SETPOINT:
                print('Enter New Setpoint:')
                toUpdate = reference
                lowerlim = -200
                upperlim = 200
                state = S14_INPUTNUM
                yield None
            elif state == S13_GAINS:
                print('Enter New K_pOUT:')
                toUpdate = K_pOUT
                lowerlim = 0
                upperlim = 100
                state = S14_INPUTNUM
                yield None
            elif state == S14_INPUTNUM:
                buffer = ''
                while True:
                    if serport.any():
                        charIn = serport.read(1).decode()
                        if charIn.isdigit():
                            buffer+=charIn
                        elif charIn == '-':
                            if buffer == '':
                                buffer +=charIn
                        elif charIn == '.':
                            if buffer.find('.') == -1:
                                buffer += charIn
                        elif charIn == '\x7F':
                            if buffer != '':
                                buffer = buffer[:-1]
                        elif charIn in {'\r', '\n'}:
                            if buffer == '':
                                buffer = str(toUpdate.read())
                            break
                        print(buffer)
                        yield None
                    else:
                        yield None
                if float(buffer)>=lowerlim and float(buffer)<=upperlim:
                    toUpdate.write(float(buffer))
                    print('Updated!')
                else:
                    print('User input outside acceptable bounds.')
                    print(f'Min= {lowerlim}')
                    print(f'Max: {upperlim}')
                    state = S1_WAIT
                    toUpdate = None
                    
                if toUpdate == K_pOUT:
                    toUpdate = K_iOUT
                    lowerlim = 0
                    upperlim = 500
                    print('Enter New K_iOUT:')
                    state = S14_INPUTNUM
                elif toUpdate == K_iOUT:
                    toUpdate = K_dOUT
                    lowerlim = 0
                    upperlim = 500
                    print('Enter New K_dOUT:')
                    state = S14_INPUTNUM
                elif toUpdate == K_dOUT:
                    toUpdate = K_pIN
                    lowerlim = 0
                    upperlim = 100
                    print('Enter New K_pIN:')
                    state = S14_INPUTNUM
                elif toUpdate == K_pIN:
                    toUpdate = K_iIN
                    lowerlim = 0
                    upperlim = 100
                    print('Enter New K_iIN:')
                    state = S14_INPUTNUM
                elif toUpdate == K_iIN:
                    toUpdate = K_dIN
                    lowerlim = 0
                    upperlim = 100
                    print('Enter New K_dIN:')
                    state = S14_INPUTNUM
                else:
                    state = S1_WAIT
                yield None
            elif state == S15_CLOSEDLOOP:
                closedloopON.write(not closedloopON.read())
                cl = None
                if closedloopON.read():
                    cl = 'On'
                else:
                    cl = 'Off'
                print('Closed Loop Control ' + cl)
                state = S1_WAIT
                yield None
        else:
            yield None
    
    
def printmenu():
    '''!@brief      This task prints a menu to PuTTy informing the user of the
                    commands they may use.
    '''
    print('+----------------------------------------------------------------------------+')
    print('|--Press p or P to print IMU and Touch Panel positions-----------------------|')
    print('|--Press v or V to print IMU and Touch Panel velocities----------------------|')
    print('|--Press z or Z to zero the IMU----------------------------------------------|')
    print('|--Press y or Y to select a setpoint for closed-loop control-----------------|')
    print('|--Press k or K to select new gains for closed-loop control------------------|')
    print('|--Press w or W to turn closed loop control on or off------------------------|')
    print('|--Press g or G to collect 30 seconds of data and print as csv---------------|')
    print('|--Press s or S to end data collection---------------------------------------|')
    print('+----------------------------------------------------------------------------+')