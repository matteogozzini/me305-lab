
'''!@file Lab4Page.py

   @page lab_4_page Deliverables For Lab 4
   @details        This page contains all the FSM's and plots for Lab 4
   @section Intro
   This lab is a modification of Lab3. In this Lab we are able to control the speed of the motor
   in a closed loop control. This basically means that the user enters a desired speed, Kp, and Ki
   values(these values determine duty cycle) that allow the motor to ramp up to the desired speed safely while at the same time minimizing
   error. 
   



   
   Velocity Vs Time Plot
   @image html VelvsTime.png width=420
   NOTE: Series 3 corresponds to a Kp of 0.15 and a Ki of 10
   <br>
   This Plot shows how the inputted Ki and Kp values determine the oscilallation of the motor velocity
   which  eventually smooth out and settle to the desired user selected velocity. This Plot holds Ki constant
   while changing the value of Kp to show how steep the response time is as well as the decrese in oscillations 
   as the Kp value decreases. This graph also shows the error and settling time for each run.
   
   
   Duty Cycle Vs Time Plot
   @image html DutyVSTime.png width=420
   <br>
   This Plot shows how to inputted Ki and Kp values determine the oscillation of Duty cycle for the motor.
   The duty cycles eventually smooth out to around 55% duty cycle (which corresponds to around 100 rad/s)
   as with the graph above. In both graphs the oscillations are maximized with a Kp of 0.75, and once the Kp value 
   decreases to around 0.25, there is practically no oscillations visible and the gain is much smoother.
    
   Block Diagram 
   @image html BlockDiaLab4.png width=420
   <br>

   Task Diagram
   @image html TaskDiaLab4.png width=420
   <br>
    
   Encoder Task FSM
   @image html EncoderLab3.png width=420
   <br>
    
   User Task FSM
   @image html UserLab3.png width=420
   <br>
    
   Motor Task FSM
   @image html MotorLab3.png width=420
   <br>
    
   Controller Task FSM
   @image html ControllerTask.png width=420
   <br>

'''