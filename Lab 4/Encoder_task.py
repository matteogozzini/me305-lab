"""!@file           Encoder_task.py
    @brief          Module that communicates with the  encoder and collects data
    @details        Responsible for communicating and data sharing with the Encoder_Lab3.py file.
                    Creates encoder objects and is also responsible for zeroing and collecting data for encoders
    @author         Matteo Gozzini
    @date           2/17/22
"""

from time import ticks_us, ticks_add, ticks_diff, ticks_ms
import micropython
#import encoder
#import pyb

##Variable will be the state 0 for starting the program
S0_INIT = micropython.const(0)

##Variable will be the state 1 for updating the program
S1_UPDATE = micropython.const (1)

##Variable will be the state 2 for zeroing the program
S2_ZERO = micropython.const(2)

def updateFunction(taskName, period, zFlag, encData, ENC1, deltaTime, CLC):
    '''! @brief Updates and zeros the encoder.
         @details Collects all values from the encoder.
         @param taskName Names the task so that multiple tasks can be run simultaneously.
         @param period Passes in the period at which the code is run.
         @param zFlag A shared parameter that indicates if the 'z' key has been pressed.
         @param encData A tuple containing time, position, and delta of the encoder.
         @param ENC1 passes in the values from the encoder module.
    '''
    
    ##Variable will be used for setting the states
    state = S0_INIT
    
    ##Variable set to be the starting time in micro seconds
    start_time = ticks_us()
    
    
    ##Variable set to be the next time which will be subtracted from the current_time
    next_time = ticks_add(start_time, period)  

     
    while True:
    
        ##Variable set to ticks_us() within the loop to continue to set a new time stamp.
        current_time = ticks_us()
        if ticks_diff(current_time, next_time) >= 0:
            
            if state == S0_INIT:
                state = S1_UPDATE
                
            elif state == S1_UPDATE:
                ENC1.update()
                encData.write((ticks_ms(), ENC1.get_position(), ENC1.get_delta(), ENC1.get_vel(), CLC.get_Actuation()))
                deltaTime.write(ENC1.get_deltaTime())
                if zFlag.read():        
                    state = S2_ZERO
                    
            elif state == S2_ZERO:
                ENC1.zero()
                zFlag.write(False)
                state = S1_UPDATE
                
            else:
                pass
            next_time = ticks_add(next_time, period)
                
            yield state
        else:
            
            yield None