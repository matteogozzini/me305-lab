"""!@file          MotorDriver.py
    @brief         File for the motor which contains the Driver and Motor Class that controls dduty cycle of the motor
    @details       Enables and disables the motor. Also checks for faults and returns motor objects 
    @author        Matteo Gozzini
    @date          2/17/22
"""

from pyb import Pin, ExtInt, Timer
import time

class DRV8847:
    '''!@brief A motor driver class for the DRV8847 from TI.
        @details Objects of this class can be used to configure the DRV8847
                 motor driver and to create one or more objects of the
                 Motor class which can be used to perform motor
                 control.
                 
    '''

    def __init__ (self, timer, nSLEEP, nFAULT):
        '''!@brief Initializes and returns a DRV8847 object.
            @details Creates a driver, which can control the enabling and disabling of
            the motor.  
            @param timer The value set for which timer to use
            @param nSLEEP Allows the motor to enable or disable
            @param nFAULT Recognizes if a fault occurs within the chip
        '''
        self.timer = timer
        self.nSLEEP = nSLEEP
        self.nFAULT = nFAULT
        self.FaultInt = ExtInt(self.nFAULT, mode=ExtInt.IRQ_FALLING,
                               pull=Pin.PULL_NONE, callback=self.fault_cb)

    def enable (self):
        '''!@brief Brings the DRV8847 out of sleep mode.
            @details Enables DRV8847.  Temporarily disables fault triggering to fix
                     a bug in which the sensor would fault upon enabling.  
        '''        
        self.FaultInt.disable()
        self.nSLEEP.high()
        time.sleep_us(50)
        self.FaultInt.enable()

    def disable (self):
        '''!@brief Puts the DRV8847 in sleep mode.
            @details Disables DRV8847, stopping any motor motion.  
        '''
        print('Disabling motor')
        self.nSLEEP.low()
    
    def fault_cb (self, IRQ_src):
        '''!@brief Callback function to run on fault condition.
            @details If a fault is triggered, this is the function that is called.  
                     This function gets DRV8847 to disable the motors.  
            @param IRQ_src The source of the interrupt req
        '''
        print('Fault detected')
        self.disable()

    def motor (self, InA, InB, ChX, ChY):
        '''!@brief Creates a DC motor object connected to the DRV8847.
            @details Creates an object which the motor class can furth specify.
                     Allows multiple motors to be controlled by just one DRV8847.  
            @param InA The value of Pin A to be passed into the motor module
            @param InB The value of Pin B to be passed into the motor module
            @param ChX The value of Channel X to be passed into the motor module
            @param ChY The value of Channel Y to be passed into the motor module
            @return Motor() An object in which the motor class can further control
        '''

        return Motor(self.timer, InA, InB, ChX, ChY)
    
class Motor:
    
    '''!@brief A motor class for one channel of the DRV8847.
        @details Objects of this class can be used to apply PWM to a given
                 DC motor.

    '''
    def __init__ (self, PWM_tim, IN1_pin, IN2_pin, CH_X, CH_Y):
        
        '''!@brief Initializes and returns an object associated with a DC Motor.
            @details Objects of this class should not be instantiated
                     directly. Instead create a DRV8847 object and use
                     that to create Motor objects using the method
                     DRV8847.motor().
            @param PWM_tim Placeholder for the timer of the motor
            @param IN1_pin Placeholder for the first pin to be passed in
            @param IN2_pin Placeholder for the second pin to be passed in 
            @param CH_X Placeholder for the first channel of the timer passed in
            @param CH_Y Placeholder for the second channel of the timer passed in
        '''

        
        # Class var PWM_tim is equal to the value of input arg PWM_tim.
        self.PWM_tim = Timer(PWM_tim, freq = 20_000)
        
        # Class vars IN_pin are equal to the values of input args PWM_tim.
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        
        self.PWM_tim_CH_X = self.PWM_tim.channel(CH_X, Timer.PWM_INVERTED, pin=self.IN1_pin)
        self.PWM_tim_CH_Y = self.PWM_tim.channel(CH_Y, Timer.PWM_INVERTED, pin=self.IN2_pin)
    
    def set_duty (self, duty):        
        '''!@brief Set the PWM duty cycle for the motor channel.
            @details This method sets the duty cycle to be sent
                     to the motor to the given level. Positive values
                     cause effort in one direction, negative values
                     in the opposite direction.
            @param duty A signed number holding the duty
                        27 cycle of the PWM signal sent to the motor
        '''
        
        if duty >= 0:
            self.PWM_tim_CH_X.pulse_width_percent(0)
            self.PWM_tim_CH_Y.pulse_width_percent(duty)
        elif duty < 0:
            self.PWM_tim_CH_X.pulse_width_percent(-duty)
            self.PWM_tim_CH_Y.pulse_width_percent(0)    