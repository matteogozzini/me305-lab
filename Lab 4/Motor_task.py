"""!@file           Motor_task.py
    @brief          Assigns pins to motors, checks for user input to disable a fault, and sets the duty cycle for the motors
    @details        assigns the correct pins to each motor, sets motor duty cycle based off user input, and checks to see if user wants to disable and motor fault 
    @author         Matteo Gozzini
    @date           2/17/22
"""

from time import ticks_us, ticks_add, ticks_diff
import micropython

##Variable will be the state 0 for starting the program
S0_INIT = micropython.const(0)

##Variable will be the state 1 for waiting for the command
S1_WAIT = micropython.const (1)


def motorFunction(taskName, period, motor, dutyVal):
    '''! @brief Generator function that passes the duty cycles to the motor.
         @details This function passes the duty cycle to the method setduty which then
                  sends the value to the motor. 
         @param taskName Names the task so that multiple tasks can be run simultaneously.
         @param period Passes in the period at which the code is run.
         @param motor A shared parameter that indicates which motor to work with.
         @param dutyVal A shared parameter that indicates the value of th duty cycle.   
    '''
    
    ##Variable will be used for setting the states
    state = S0_INIT
    
    ##Variable set to be the starting time in micro seconds
    start_time = ticks_us()
    
    ## Variable set to be the next time which will be subtracted from the current_time
    next_time = ticks_add(start_time, period)
    
    motorNum = motor
     
    while True:
    
        ##Variable set to ticks_us() within the loop to continue to set a new time stamp.
        current_time = ticks_us()
        if ticks_diff(current_time, next_time) >= 0:
            
            if state == S0_INIT:
                state = S1_WAIT                
                
            elif state == S1_WAIT:
                motorNum.set_duty(dutyVal.read())
                    
            else:
                pass
            
            next_time = ticks_add(next_time, period)
                
            yield state

        else:
            
            yield None