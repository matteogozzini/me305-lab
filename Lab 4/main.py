"""!@file           main.py
    @brief          Main Program 
    @details        This main script utilizes the task files (user_task, motor_task & encoder_task) in order to 
                    communicate with the motor encoder through the encoder_lab3.py file with a set frequency 
                    for the update rate in order to get the encoders position
    @author         Matteo Gozzini
    @date           2/17/22
"""

import Encoder_task
import User_task
import Motor_task
import Controller_task
import shares
import MotorDriver
import encoder
import closedloop
from pyb import Pin

##This variable is used in conjunction with the shares class to enable the two tasks to communicate when to change states. 
zFlag = shares.Share(False)

##This variable is used in conjunction with the shares class to enable the two tasks to communicate when to change states.
cFlag = shares.Share(False)

##This variable is used in conjunction with the shares class to enable the two tasks to communicate when to change states.  
eFlag = shares.Share(False)

##This variable is used in conjunction with the shares class to enable the two tasks to communicate when to change states.  
kFlag = shares.Share(False)

##The variable allows a value duty1 to read and write when filled in the encoder module
duty1 = shares.Share(0)

##The variable allows a value duty2 to read and write when filled in the encoder module 
duty2 = shares.Share(0)

##Creates a tuple containing time, position, delta, and velocity, all obtained from the Encoder Task
encData = shares.Share((0,0,0,0,0))

KpShare = shares.Share(0)

KiShare = shares.Share(0)

yShare = shares.Share(0)

wFlag = shares.Share(False)

gain = shares.Share(0)

deltaTime = shares.Share(0)

##Pushes forward the ENC1 variable from the encoder module
ENC1 = encoder.Encoder(Pin.cpu.B6, Pin.cpu.B7, 4)

##This object pulls in the timer and pins defined in the MotorDriver module and DRV8847 class
motor_drv = MotorDriver.DRV8847(3, Pin(Pin.cpu.A15, mode=Pin.OUT_PP), Pin(Pin.cpu.B2, mode=Pin.OUT_PP)) 

##This object pulls in the pins and channels defined in the motor class to communicate with MotorDriver
motor_1 = motor_drv.motor(Pin.cpu.B4, Pin.cpu.B5, 1, 2)

##This object pulls in the pins and channels defined in the motor class to communicate with MotorDriver
motor_2 = motor_drv.motor(Pin.cpu.B0, Pin.cpu.B1, 3, 4)

motor_drv.enable()

CLC = closedloop.ClosedLoop (KpShare, KiShare, yShare, 100, -100)



if __name__ == '__main__':
    
    ##Contains the two tasks that are used to run the User Interface.
    taskList = [User_task.taskUserFCN ('Task User', 10_000, zFlag, cFlag, eFlag, encData, duty1, duty2, KpShare, KiShare, yShare, wFlag),
                Encoder_task.updateFunction ('Task Encoder', 10_000, zFlag, encData, ENC1, deltaTime, CLC),
                Motor_task.motorFunction ('Task Motor 1', 10_000, motor_1, duty1),
                Motor_task.motorFunction ('Task Motor 2', 10_000, motor_2, duty2),
                Controller_task.loopFunction('Task Closed Loop', 10_000, kFlag, CLC, encData, wFlag, KpShare, KiShare, yShare, duty1, deltaTime)]
    
    while True:
        try:
            for task in taskList:
                next(task)
        except KeyboardInterrupt:
            motor_drv.disable() 
            break
    print('Stopping Motor')